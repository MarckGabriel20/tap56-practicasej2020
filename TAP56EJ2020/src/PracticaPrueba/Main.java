
package PracticaPrueba;

import javax.swing.JButton;
import javax.swing.JFrame;

public class Main {

    public static void main(String[] args) {
        Main app = new Main();
        app.ventana();
    }

    public void ventana() {
        JFrame frm = new JFrame("Saludador");
        frm.setSize(500, 400);
        frm.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frm.setLocationRelativeTo(null);
        
        
        JButton boton1 = new JButton("¡Saludar!");
        frm.add(boton1);
        frm.setVisible(true);
    }
}


