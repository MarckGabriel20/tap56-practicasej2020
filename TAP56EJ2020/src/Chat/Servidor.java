package Chat;

import java.io.DataInputStream;
import java.net.ServerSocket;
import java.net.Socket;
import javax.swing.JOptionPane;

/**
 *
 * @author ingma
 */
public class Servidor extends javax.swing.JFrame {
    
    private ServerSocket server;
    private final int PUERTOH = 1000;
    
    public Servidor() {
        initComponents();
        
        try {
            server = new ServerSocket(PUERTOH); //Recepciona el puerto para la conexión
            mensajeria("* . :Servidor con Conexión: . * \n");
            
            super.setVisible(true); //Nos permite ver al servidor y lo pone a correr

            while (true) {
                Socket cliente = server.accept();
                mensajeria("Cliente conectado desde la dirección: " + cliente.getInetAddress().getHostAddress()); //Se captura la dirección IP del cliente que nombre
                
                DataInputStream entrada = new DataInputStream(cliente.getInputStream());
                
                HiloServidor hilo = new HiloServidor(cliente, entrada.readUTF(), this);
                hilo.start();
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(this, e.toString());
        }
    }
    
    public void mensajeria(String msj) {
        this.jTextArea1.append("  " + msj + "\n");// El .append es el que muestra el mensaje
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane2 = new javax.swing.JScrollPane();
        jTextArea1 = new javax.swing.JTextArea();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jScrollPane2.setBorder(javax.swing.BorderFactory.createTitledBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 255, 255), 3, true), "SERVIDOR", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Times New Roman", 3, 14), new java.awt.Color(255, 0, 51))); // NOI18N

        jTextArea1.setColumns(20);
        jTextArea1.setRows(5);
        jScrollPane2.setViewportView(jTextArea1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 400, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 482, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        
        new Servidor();
        
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTextArea jTextArea1;
    // End of variables declaration//GEN-END:variables
}
