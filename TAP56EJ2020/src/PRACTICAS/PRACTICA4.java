
package PRACTICAS;

import java.awt.event.KeyEvent;

public class PRACTICA4 extends javax.swing.JFrame {

    public PRACTICA4() {
        initComponents();
        buttonGroup1.add(this.RBOriginal1);
        buttonGroup1.add(this.RBOriginal2);
        buttonGroup1.add(this.RBOriginal3);
        
        buttonGroup2.add(this.RBEspejo4);
        buttonGroup2.add(this.RBEspejo5);
        buttonGroup2.add(this.RBEspejo6);
    }

    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        buttonGroup2 = new javax.swing.ButtonGroup();
        jLabel1 = new javax.swing.JLabel();
        RBOriginal1 = new javax.swing.JRadioButton();
        RBOriginal2 = new javax.swing.JRadioButton();
        RBOriginal3 = new javax.swing.JRadioButton();
        CHBOriginal1 = new javax.swing.JCheckBox();
        CHBOriginal2 = new javax.swing.JCheckBox();
        CHBOriginal3 = new javax.swing.JCheckBox();
        jTextField1 = new javax.swing.JTextField();
        jComboBox1 = new javax.swing.JComboBox<>();
        jSpinner1 = new javax.swing.JSpinner();
        jSeparator1 = new javax.swing.JSeparator();
        jLabel2 = new javax.swing.JLabel();
        RBEspejo4 = new javax.swing.JRadioButton();
        RBEspejo5 = new javax.swing.JRadioButton();
        RBEspejo6 = new javax.swing.JRadioButton();
        CHBEspejo4 = new javax.swing.JCheckBox();
        CHBEspejo5 = new javax.swing.JCheckBox();
        CHBEspejo6 = new javax.swing.JCheckBox();
        jTextField2 = new javax.swing.JTextField();
        jComboBox2 = new javax.swing.JComboBox<>();
        jSpinner2 = new javax.swing.JSpinner();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jLabel1.setText("Original");

        RBOriginal1.setText("Opcion 1");
        RBOriginal1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                RBOriginal1ActionPerformed(evt);
            }
        });

        RBOriginal2.setText("Opcion 2");
        RBOriginal2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                RBOriginal2ActionPerformed(evt);
            }
        });

        RBOriginal3.setText("Opcion 3");
        RBOriginal3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                RBOriginal3ActionPerformed(evt);
            }
        });

        CHBOriginal1.setText("Opcion 4");
        CHBOriginal1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CHBOriginal1ActionPerformed(evt);
            }
        });

        CHBOriginal2.setText("Opcion 5");
        CHBOriginal2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CHBOriginal2ActionPerformed(evt);
            }
        });

        CHBOriginal3.setText("Opcion 6");
        CHBOriginal3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CHBOriginal3ActionPerformed(evt);
            }
        });

        jTextField1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTextField1KeyReleased(evt);
            }
        });

        jSpinner1.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                jSpinner1StateChanged(evt);
            }
        });

        jLabel2.setText("Espejo");
        jLabel2.setEnabled(false);

        RBEspejo4.setText("Opcion 1");
        RBEspejo4.setEnabled(false);

        RBEspejo5.setText("Opcion 2");
        RBEspejo5.setEnabled(false);

        RBEspejo6.setText("Opcion 3");
        RBEspejo6.setEnabled(false);

        CHBEspejo4.setText("Opcion 4");
        CHBEspejo4.setEnabled(false);

        CHBEspejo5.setText("Opcion 5");
        CHBEspejo5.setEnabled(false);

        CHBEspejo6.setText("Opcion 6");
        CHBEspejo6.setEnabled(false);

        jTextField2.setEnabled(false);

        jComboBox2.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { " " }));
        jComboBox2.setEnabled(false);

        jSpinner2.setEnabled(false);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jSeparator1)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel1)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(RBOriginal1)
                                    .addComponent(RBOriginal2)
                                    .addComponent(RBOriginal3))
                                .addGap(42, 42, 42)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(CHBOriginal1)
                                    .addComponent(CHBOriginal2)
                                    .addComponent(CHBOriginal3))
                                .addGap(40, 40, 40)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(jComboBox1, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(jSpinner1, javax.swing.GroupLayout.DEFAULT_SIZE, 150, Short.MAX_VALUE)
                                    .addComponent(jTextField1)))
                            .addComponent(jLabel2)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(RBEspejo4)
                                    .addComponent(RBEspejo5)
                                    .addComponent(RBEspejo6))
                                .addGap(35, 35, 35)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(CHBEspejo4)
                                    .addComponent(CHBEspejo5)
                                    .addComponent(CHBEspejo6))
                                .addGap(50, 50, 50)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(jTextField2)
                                    .addComponent(jComboBox2, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(jSpinner2, javax.swing.GroupLayout.DEFAULT_SIZE, 150, Short.MAX_VALUE))))
                        .addGap(0, 11, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(RBOriginal1)
                    .addComponent(CHBOriginal1)
                    .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(RBOriginal2)
                    .addComponent(CHBOriginal2)
                    .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(RBOriginal3)
                    .addComponent(CHBOriginal3)
                    .addComponent(jSpinner1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(RBEspejo4)
                    .addComponent(CHBEspejo4)
                    .addComponent(jTextField2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(RBEspejo5)
                    .addComponent(CHBEspejo5)
                    .addComponent(jComboBox2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(RBEspejo6)
                    .addComponent(CHBEspejo6)
                    .addComponent(jSpinner2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(29, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jTextField1KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField1KeyReleased
        if (evt.getKeyCode()==KeyEvent.VK_ENTER ){
            this.jComboBox1.addItem(this.jTextField1.getText());
        } else {
            this.jTextField2.setText(this.jTextField1.getText());
        }
    }//GEN-LAST:event_jTextField1KeyReleased

    private void jSpinner1StateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_jSpinner1StateChanged
       this.jSpinner2.setValue(this.jSpinner1.getValue());
    }//GEN-LAST:event_jSpinner1StateChanged

    private void RBOriginal1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_RBOriginal1ActionPerformed
        this.RBEspejo4.setSelected(this.RBOriginal1.isSelected());
    }//GEN-LAST:event_RBOriginal1ActionPerformed

    private void RBOriginal2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_RBOriginal2ActionPerformed
       this.RBEspejo5.setSelected(this.RBOriginal2.isSelected());
    }//GEN-LAST:event_RBOriginal2ActionPerformed

    private void RBOriginal3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_RBOriginal3ActionPerformed
       this.RBEspejo6.setSelected(this.RBOriginal3.isSelected());
    }//GEN-LAST:event_RBOriginal3ActionPerformed

    private void CHBOriginal1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CHBOriginal1ActionPerformed
        this.CHBEspejo4.setSelected(this.CHBOriginal1.isSelected());
    }//GEN-LAST:event_CHBOriginal1ActionPerformed

    private void CHBOriginal2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CHBOriginal2ActionPerformed
       this.CHBEspejo5.setSelected(this.CHBOriginal2.isSelected());
    }//GEN-LAST:event_CHBOriginal2ActionPerformed

    private void CHBOriginal3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CHBOriginal3ActionPerformed
        this.CHBEspejo6.setSelected(this.CHBOriginal3.isSelected());
    }//GEN-LAST:event_CHBOriginal3ActionPerformed

   
    public static void main(String args[]) {
        
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new PRACTICA4().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JCheckBox CHBEspejo4;
    private javax.swing.JCheckBox CHBEspejo5;
    private javax.swing.JCheckBox CHBEspejo6;
    private javax.swing.JCheckBox CHBOriginal1;
    private javax.swing.JCheckBox CHBOriginal2;
    private javax.swing.JCheckBox CHBOriginal3;
    private javax.swing.JRadioButton RBEspejo4;
    private javax.swing.JRadioButton RBEspejo5;
    private javax.swing.JRadioButton RBEspejo6;
    private javax.swing.JRadioButton RBOriginal1;
    private javax.swing.JRadioButton RBOriginal2;
    private javax.swing.JRadioButton RBOriginal3;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.ButtonGroup buttonGroup2;
    private javax.swing.JComboBox<String> jComboBox1;
    private javax.swing.JComboBox<String> jComboBox2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSpinner jSpinner1;
    private javax.swing.JSpinner jSpinner2;
    private javax.swing.JTextField jTextField1;
    private javax.swing.JTextField jTextField2;
    // End of variables declaration//GEN-END:variables
}
