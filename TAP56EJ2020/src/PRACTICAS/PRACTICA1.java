
package PRACTICAS;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

public class PRACTICA1 extends JFrame implements ActionListener {

    JTextField text;

    PRACTICA1() {
        setTitle("SALUDADOR");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(300, 150);
        setLocationRelativeTo(null);

        setLayout(new FlowLayout());

        JLabel panel = new JLabel("Escribie un nombre");

        text = new JTextField(20);

        JButton boton = new JButton("¡Saludar!");
        
        boton.addActionListener(this);
        
        add(panel);
        add(text);
        add(boton);
    }

    public static void main(String[] args) {
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new PRACTICA1().setVisible(true);
            }
        });
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        JOptionPane.showMessageDialog(this, "Hola "+text.getText());
    }
}